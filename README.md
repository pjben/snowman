# TP Bases HTML CSS JS

## Récupérer les sources et créer votre branche personnelle
- Cloner le projet git : 
`git clone https://gitlab.isima.fr/dev-web-2AF2/rappel-html.git`
- Créer une branche :
`git branch 2022-<nom de famille>`
`git checkout 2022-<nom de famille>`
- Soumettre la branche :
`git push -u origin 2022-<nom de famille>`

## Etapes du TD

Le but est de reproduire la scène [bonhomme.gif](./bonhomme.gif) avec des éléments HTML uniquement.

### Créer un cercle avec Chrome devtools
  - ouvrir une page blanche Chrome et ouvrir les outils de développement (F12)
  - insérer une div HTML
  - modifier le CSS pour ajouter une taille fixe et une bordure
  - arrondir la div
  - positionner le cercle en bas au centre avec flex-box (https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

### Structurer le code
  - séparer le code en 3 fichiers : .html, .css et .js
  - le fichier `bonhomme.html` importe le fichier `bonhomme.css` avec la balise `<link rel="stylesheet" href="bonhomme.css">` à l'intérieur de `<head>`
  - le fichier `bonhomme.html` importe le fichier `bonhomme.js` avec la balise `<script src="bonhomme.js"></script>` à toute en bas du fichier, avant `</html>`
  - ouvrir `bonhomme.html` avec Chrome pour vérifier que la page s'affiche correctement

### Créer les autres éléments
  - ajout fond d'écran [background.jpg](./background.jpg)
  - ajouter les différentes parties du bonhomme à l'aide de div
    - bonus : dessiner la bouche et le nez en SVG, par exemple avec [figma.com](https://figma.com)

### Ajouter dynamiquement des flocons avec Javascript

#### Sous Chrome devtools
  - ouvrez la console javascript
  - récupérer votre élément body avec `document.querySelector` 
  - ajouter-lui un flocon dynamiquement avec `document.createElement` et `appendChild`

#### Dans un éditeur
  - recopier votre code dans le fichier `bonhomme.js`
  - génerer 20 flocons avec une boucle
  - faire en sorte que les flocons aient une position aléatoire
    - bonus : certains flocons passent devant, d'autres derrière le bonhomme
  
### Animer le contenu en Javascript
  - mettez au point une fonction qui fait tomber les flocons
  - utiliser `setInterval` pour faire tomber les flocons en continu et en génerer de nouveaux


