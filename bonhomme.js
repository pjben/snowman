setInterval(generateSnow, 2000);
setInterval(putDownSnow, 200);

function generateSnow() {
    for (let i = 0; i < 20; i++) {
        const div = document.createElement('div');
        div.className = 'flocon blue';
        div.style.top = '1px';
        div.style.left = randomPercentage();  // 0% < left < 100%
        div.style.zIndex = randomBoolean() ? -1 : 4; // -1 < zIndex < 4 
        document.querySelector('.snow').append(div);    
    }
}

function putDownSnow() {
    document.querySelectorAll('.flocon').forEach(el => {
        el.style.top = addPixels(el.style.top, Math.random()*20);
    });
}

function randomBoolean() {
    return Math.round(Math.random()) === 0;
}

function randomPercentage() {
    return Math.random() * 100 + '%';
}

function addPixels(pixelString, pixelsToAdd) {              // pixelString = '1px', pixelsToAdd = 2
    const splitted = pixelString.split(/px/);               // splitted = ['1']
    const stringValue = splitted[0];                        // stringValue = '1'
    const numberValue = stringValue * 1;                    // numberValue = 1
    return (numberValue + pixelsToAdd) + 'px';              // return = '3px'

}
